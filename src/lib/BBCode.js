export class BBCode {
  constructor(codes) {
    this.codes = [];
    this.setCodes(codes);
  }

  parse(text) {
    return this.codes.reduce((text, code) => text.replace(code.regexp, code.replacement), text);
  }

  add(regex, replacement) {
    this.codes.push({
      regexp: new RegExp(regex, 'gmis'),
      replacement: replacement,
    });
    return this;
  }

  setCodes(codes) {
    this.codes = Object.keys(codes).map(function (regex) {
      const replacement = codes[regex];
      return {
        regexp: new RegExp(regex, 'gmis'),
        replacement: replacement,
      };
    }, this);
    return this;
  }
}
