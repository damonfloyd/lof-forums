import { BBCode } from './BBCode';

const bbCodeParser = new BBCode({
  '\\[b\\](.+?)\\[/b\\]': '<span class="font-bold">$1</span>',
  '\\[i\\](.+?)\\[/i\\]': '<span class="italic">$1</span>',
  '\\[u\\](.+?)\\[/u\\]': '<span class="underline">$1</span>',
  '\\[s\\](.+?)\\[/s\\]': '<span class="line-through">$1</span>',
  '\\[url\\](.+?)\\[/url\\]': '<a href="$1" target="_blank">$1</a>',
  '\\[quote=\\"(.*?)\\"\\spost=\\"(.*?)\\"\\](.+?)\\[\\/quote\\]':
    '<div class="quote-block"><div class="quote-author">$1 posted:</div>$3</div>',
  '\\[quote=\\"(.*?)\\](.+?)\\[\\/quote\\]': '<div class="quote-block">$2</div>',
  '\\[quote\\](.+?)\\[\\/quote\\]': '<div class="quote-block">$1</div>',
});

// remove formatting, and remove quote blocks entirely
const bbCodeStripper = new BBCode({
  '\\[b\\](.+?)\\[/b\\]': '$1',
  '\\[i\\](.+?)\\[/i\\]': '$1',
  '\\[u\\](.+?)\\[/u\\]': '$1',
  '\\[s\\](.+?)\\[/s\\]': '$1',
  '\\[url\\](.+?)\\[/url\\]': '$1',
  '\\[quote=\\"(.*?)\\"\\spost=\\"(.*?)\\"\\](.+?)\\[\\/quote\\]': '',
  '\\[quote=\\"(.*?)\\](.+?)\\[\\/quote\\]': '',
  '\\[quote\\](.+?)\\[\\/quote\\]': '',
});

const parse = (input) => {
  return bbCodeParser.parse(input);
};

const strip = (input) => {
  return bbCodeStripper.parse(input);
};

export default { parse, strip };
