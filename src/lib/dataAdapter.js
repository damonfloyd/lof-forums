import axios from 'axios';

axios.defaults.validateStatus = function () {
  return status < 500;
};

const instance = axios.create({
  baseURL: 'http://localhost:3000',
  headers: { 'x-aok-user': -1 },
});

instance.interceptors.request.use((req) => {
  if (req.url && req.url.indexOf('login') === -1) {
    const token = localStorage.getItem('token');
    if (token !== '') {
      req.headers.authorization = `Bearer ${token}`;
    }
    const id = parseInt(localStorage.getItem('id') || '-1');
    if (id !== -1) {
      req.headers['x-aok-user'] = id;
    }
  }
  return req;
});

instance.interceptors.response.use(
  (res) => res,
  (err) => {
    if (err.response) {
      console.log(err);
      switch (err.response.status) {
        case 401:
          throw new Error('your session has expired');
        default: {
          throw new Error('unknown error');
        }
      }
    } else {
      console.log('no response:', err);
      throw new Error(err);
    }
  }
);

export default {
  async execute(method, url, data = undefined) {
    let config = { method, url };
    if (data) {
      config.data = data;
    }
    return instance.request(config);
  },

  async get(url) {
    return this.execute('get', url);
  },

  async post(url, payload) {
    return this.execute('post', url, payload);
  },

  async put(url, payload) {
    return this.execute('put', url, payload);
  },

  async delete(url) {
    return this.execute('delete', url);
  },
};
