import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import i18n from './i18n';
import './assets/tailwind.css';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import './fontawesome';

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;

Vue.mixin({
  methods: {
    robot: function (title, message) {
      if (this.$store) {
        this.$store.commit('NOTIFY_ADD', { title, message });
      } else {
        console.warn('robot has no store!');
      }
    },
  },
});

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
