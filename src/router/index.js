import Vue from 'vue';
import VueRouter from 'vue-router';

import Forum from '@/views/Forum.vue';
import ForumsIndex from '@/views/ForumsIndex';
import Login from '@/views/Login.vue';
import NoPerms from '@/views/NoPerms.vue';
import ReplyThread from '@/views/ReplyThread.vue';
import Signup from '@/views/Signup.vue';
import Thread from '@/views/Thread.vue';
import UserProfile from '@/views/UserProfile.vue';

import store from '@/store/index';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
  },
  {
    path: '/index',
    name: 'Index',
    component: ForumsIndex,
    meta: {
      breadcrumb: 'ForumIndex',
    },
  },
  {
    path: '/user/:id',
    component: {
      render(c) {
        return c('router-view');
      },
    },
    meta: {
      breadcrumb: 'viewUser',
    },
    children: [
      {
        path: '',
        component: UserProfile,
        name: 'User',
        meta: {
          breadcrumb: 'viewOneUser',
        },
      },
    ],
  },
  {
    path: '/forum/:id',
    component: {
      render(c) {
        return c('router-view');
      },
    },
    meta: {
      breadcrumb: 'viewForum',
    },
    children: [
      {
        path: '',
        component: {
          render(c) {
            return c('router-view');
          },
        },
        meta: {
          breadcrumb: 'viewOneForum',
        },
        children: [
          {
            path: '',
            component: Forum,
            name: 'Forum',
            meta: {
              breadcrumb: 'viewOneForum',
            },
            // beforeEnter: (to, from, next) => {
            //   if(UserStore.user.isAdmin) {
            //     next();
            //   }
            //   else {
            //     // get permitted forums
            //     const permittedForums = UserStore.permitForums;
            //     const forumId = parseInt(to.params.id);
            //     if(permittedForums.find(e => e === forumId)) {
            //       next();
            //     }
            //     else {
            //       next({
            //         name: 'NoPerms',
            //         query: { redirectFrom: to.fullPath }
            //       });
            //     }
            //   }
            // }
          },
          {
            path: 'newthread',
            component: ReplyThread,
            meta: {
              breadcrumb: 'viewReplyThread',
            },
          },
        ],
      },
      {
        path: 'thread/:threadId',
        component: {
          render(c) {
            return c('router-view');
          },
        },
        meta: {
          breadcrumb: 'viewThread',
        },
        children: [
          {
            path: '',
            component: Thread,
            meta: {
              breadcrumb: 'viewOneThread',
            },
          },
          {
            path: 'reply',
            name: 'Reply',
            component: ReplyThread,
            meta: {
              breadcrumb: 'viewReplyThread',
            },
            props: (route) => ({ query: route.query.q }),
          },
        ],
      },
      {
        path: 'thread/:threadId',
        name: 'Thread',
        component: Thread,
        meta: {
          breadcrumb: 'viewThread',
        },
      },
    ],
  },
  // {
  //   path: '/admin',
  //   component: {
  //     render(c) {
  //       return c('router-view');
  //     },
  //   },
  //   meta: { breadcrumb: 'Admin' },
  //   children: [
  //     {
  //       path: 'colorsanimals',
  //       component: ViewAnimalsColors,
  //       meta: {
  //         breadcrumb: 'Colors & Animals',
  //       },
  //     },
  //     {
  //       path: 'animalscolors',
  //       component: ViewAnimalsColors,
  //       meta: {
  //         breadcrumb: 'Colors & Animals',
  //       },
  //     },
  //   ],
  // },
  {
    path: '/noperms',
    name: 'NoPerms',
    component: NoPerms,
    meta: {
      breadcrumb: 'Uh oh!',
    },
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.name !== 'Signup' && to.name !== 'Login') {
    const userId = store.getters['id'];
    if (userId === -1) {
      // user is logged out.
      next({
        name: 'Login',
        query: { redirectFrom: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
