import DataAdapter from '@/lib/dataAdapter.js';

export default {
  async get(id) {
    return await DataAdapter.get(`/forum/${id}`);
  },
  async getThreadsByForum(forumId, page) {
    return await DataAdapter.get(`/forum/${forumId}/threads?page=${page}`);
  },
};
