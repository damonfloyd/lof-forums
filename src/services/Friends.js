import DataAdapter from '@/lib/dataAdapter';

export default {
  accept: async (requestId) => {
    return await DataAdapter.get(`/friends/accept/${requestId}`);
  },
  add: async (targetId) => {
    return await DataAdapter.get(`/friends/request/${targetId}`);
  },
  cancel: async (requestId) => {
    return await DataAdapter.get(`/friends/cancel/${requestId}`);
  },
  decline: async (requestId) => {
    return await DataAdapter.get(`/friends/decline/${requestId}`);
  },
  get: async (userId) => {
    return await DataAdapter.get(`/friends/${userId}`);
  },
  getPending: async () => {
    return await DataAdapter.get(`/friends/pending`);
  },
};
