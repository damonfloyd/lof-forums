import DataAdapter from '@/lib/dataAdapter';

export default {
  get: async (threadId, page) => {
    return await DataAdapter.get(`/thread/${threadId}/posts?page=${page || 1}`);
  },
  getById: async (id) => {
    return await DataAdapter.get(`/post/${id}`);
  },
  getForReply: async (threadId) => {
    return await DataAdapter.get(`/thread/${threadId}/posts?r=1`);
  },
  postReply: async (post) => {
    const { authorId, threadId, content } = post;
    const data = { authorId, content };
    return await DataAdapter.post(`/thread/${threadId}/post`, data);
  },
};
