import DataAdapter from '@/lib/dataAdapter';

export default {
  getById: async (id) => {
    return await DataAdapter.get(`/thread/${id}`);
  },
  postNew: async (thread) => {
    const forumId = thread.forumId;
    return await DataAdapter.post(`/forum/${forumId}/thread`, thread);
  },
};
