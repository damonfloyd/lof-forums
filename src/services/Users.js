import DataAdapter from '@/lib/dataAdapter';

export default {
  getAuthor: async (id) => {
    return await DataAdapter.get(`/author/${id}`);
  },
  getById: async (id) => {
    return await DataAdapter.get(`/user/${id}`);
  },
  getPostInfo: async (userId) => {
    return await DataAdapter.get(`/user/${userId}/postinfo`);
  },
  signup: async (newUser) => {
    return await DataAdapter.post(`/user`, newUser);
  },
};
