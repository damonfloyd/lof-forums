import DataAdapter from '@/lib/dataAdapter';

export default {
  getAnimals: async () => {
    return await DataAdapter.get(`/animals`);
  },
  getColors: async () => {
    return await DataAdapter.get(`/colors`);
  },
};
