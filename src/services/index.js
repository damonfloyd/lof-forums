import Forums from './Forums';
import Threads from './Threads';
import Posts from './Posts';

export { Forums, Threads, Posts };
