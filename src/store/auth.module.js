import DataAdapter from '@/lib/dataAdapter';

export default {
  namespaced: true,
  state: {
    id: -1,
    name: '',
    guid: '',
    token: '',
    isAdmin: false,
  },
  getters: {
    id: (state) => state.id,
    name: (state) => state.name,
    guid: (state) => state.guid,
    token: (state) => state.token,
    isAdmin: (state) => state.isAdmin,
    user: (state) => state,
  },
  mutations: {
    SET(state, user) {
      state = Object.assign(user);
      localStorage.removeItem('token');
      localStorage.removeItem('id');
      localStorage.removeItem('isAdmin');
      localStorage.setItem('token', user.token);
      localStorage.setItem('id', user.id);
      localStorage.setItem('isAdmin', user.is_admin);
    },
    CLEAR(state) {
      state = Object.assign({
        id: -1,
        name: '',
        guid: '',
        token: '',
        isAdmin: false,
      });
      localStorage.removeItem('token');
      localStorage.removeItem('id');
      localStorage.removeItem('isAdmin');
    },
  },
  actions: {
    async login({ commit }, payload) {
      const response = await DataAdapter.post('/login', payload);
      let success = false;
      if (response.status === 200) {
        commit('SET', response.data);
        success = true;
        return { success, data: response.data };
      } else {
        throw { success, data: response.data };
      }
    },
  },
};
