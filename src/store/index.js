import Vue from 'vue';
import Vuex from 'vuex';
// import auth from './auth.module';
import DataAdapter from '@/lib/dataAdapter';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    auth: {
      id: -1,
      name: '',
      guid: '',
      isAdmin: false,
    },
    breadcrumb: {
      cacheLimit: 5,
      users: [],
      forums: [],
      threads: [],
    },
    categories: [],
    currentPage: 1,
    notifications: [],
    permitForums: [],
  },
  mutations: {
    AUTH_SET(state, auth) {
      localStorage.clear();
      localStorage.setItem('id', auth.id);
      localStorage.setItem('token', auth.token);
      localStorage.setItem('guid', auth.guid);
      localStorage.setItem('isAdmin', auth.is_admin);
      state.auth = auth;
    },
    AUTH_CLEAR(state) {
      state.auth = {
        id: -1,
        name: '',
        guid: '',
        isAdmin: false,
      };
    },
    BC_ADD_FORUM(state, forum) {
      const forums = state.breadcrumb.forums;
      forums.push({
        id: forum.id,
        name: forum.name,
        description: forum.description,
      });
    },
    BC_ADD_THREAD(state, thread) {
      const threads = state.breadcrumb.threads;
      threads.push({
        id: thread.id,
        title: thread.title,
        forumId: thread.forumId,
      });
    },
    BC_ADD_USER(state, user) {
      const users = state.breadcrumb.users;
      users.push({
        id: user.id,
        name: user.name,
      });
    },
    CATEGORIES_SET(state, categories) {
      state.categories = categories;
    },
    NOTIFY_ADD(state, notification) {
      state.notifications.push(notification);
    },
    NOTIFY_REMOVE(state, notification) {
      const idx = state.notifications.findIndex(
        (n) => n.message === notification.message && n.title === notification.title
      );
      if (idx !== -1) {
        state.notifications.splice(idx, 1);
      }
    },
    PAGE_SET(state, page) {
      state.currentPage = page;
    },
    PERMITTED_FORUMS_SET(state, forumIds) {
      state.permitForums = forumIds;
    },
  },
  getters: {
    id: (state) => state.auth.id,
    name: (state) => state.auth.name,
    guid: (state) => state.auth.guid,
    isAdmin: (state) => state.auth.isAdmin,
    user: (state) => state.auth,
    categories: (state) => state.categories,
    bcCacheLimit: (state) => state.breadcrumb.cacheLimit,
    bcUsers: (state) => state.breadcrumb.cacheLimit,
    bcForums: (state) => state.breadcrumb.cacheLimit,
    bcThreads: (state) => state.breadcrumb.cacheLimit,
    notifications: (state) => state.notifications,
    page: (state) => state.currentPage,
  },
  actions: {
    async getCategories({ commit }) {
      const response = await DataAdapter.get('/categories');
      if (response.status === 200) {
        commit('CATEGORIES_SET', response.data);
        // store all allowed forum ids, to use for restricting access
        const forumIds = [];
        for (let c of response.data) {
          if (c.forums && c.forums.length > 0) {
            for (let f of c.forums) {
              forumIds.push(f.id);
            }
          }
        }
        commit('PERMITTED_FORUMS_SET', forumIds);
      } else {
        return { message: 'unable to get categories', status: response.status };
      }
    },
    async getForum({ commit, state }, forumId) {
      const f = state.breadcrumb.forums.find(({ id }) => id === forumId);
      if (f) {
        return f;
      } else {
        let forum = null;
        try {
          const result = await DataAdapter.get(`/bc/forum/${forumId}`);
          if (result.status === 200) {
            forum = result.data;
            commit('BC_ADD_FORUM', forum);
            return forum;
          }
        } catch (e) {
          //TODO: something?
        }
      }
    },
    async getThread({ commit, state }, { threadId, fId }) {
      const t = state.breadcrumb.threads.find(({ id, forumId }) => id === threadId && forumId === fId);
      if (t) {
        return t;
      } else {
        let thread = null;
        try {
          const result = await DataAdapter.get(`/bc/forum/${fId}/thread/${threadId}`);
          if (result.status === 200) {
            thread = result.data;
            commit('BC_ADD_THREAD', thread);
            return thread;
          }
        } catch (e) {
          //TODO: something?
        }
      }
    },
    async getUser({ commit, state }, userId) {
      const u = state.breadcrumb.users.find(({ id }) => id === userId);
      if (u) {
        return u;
      } else {
        let user = null;
        try {
          const result = await DataAdapter.get(`/bc/user/${userId}`);
          if (result.status === 200) {
            user = result.data;
            commit('BC_ADD_USER', user);
            return user;
          }
        } catch (e) {
          //TODO: something?
        }
      }
    },
    async login(_ctx, payload) {
      return await DataAdapter.post('/login', payload);
    },
  },
  // strict: true,
  // modules: {
  //   auth,
  // },
});

export default store;
